﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;

namespace djrayblade
{
    public class FileRetriever
    {
        private const string ARTICLE_URL_BASE = "http://blog.naver.com/PostView.nhn?blogId=djrayblade&logNo=";

        public int MaxRetryCount { get; set; }

        public Article Article { get; private set; }

        public string ArticleUrl { get; private set; }

        public string MediaUrl { get; private set; }

        public string FileName { get; private set; }

        public string FileExtention { get; private set; }

        public FileRetriever(Article article)
        {
            MaxRetryCount = int.Parse(ConfigurationManager.AppSettings.Get("MaxRetryCount") ?? "5");

            Article = article;
            ArticleUrl = ARTICLE_URL_BASE + Article.Id;

            MediaUrl = GetPathFromPage();
            if (string.IsNullOrWhiteSpace(MediaUrl))
            {
                Logger.WriteLine("FileRetriever Initializing Failed - Not Exist Media File - " + Article);
                throw new FileNotFoundException();
            }
            FileExtention = MediaUrl[MediaUrl.Length - 4] == '.' ? MediaUrl.Substring(MediaUrl.Length - 3) : "mp3";

            FileName = Path.GetInvalidFileNameChars()
                .Aggregate($"[{article.Id}] {Article.Title}.{FileExtention}",
                    (current, chr) => current.Replace(chr, '_'));
            Logger.WriteLine("FileRetriever Initialized - " + Article);
        }

        private string GetPathFromPage()
        {
            var page = new WebClient().DownloadString(ArticleUrl);

            var position = page.IndexOf("<embed", StringComparison.OrdinalIgnoreCase);
            if (position == -1)
                return null;
            var quoteFlag = false;
            position = page.IndexOf(@"src=", position, StringComparison.OrdinalIgnoreCase) + 4;
            if (page[position] == '"')
            {
                ++position;
                quoteFlag = true;
            }

            var nextPosition = quoteFlag ? page.IndexOf('"', position) : page.IndexOf(' ', position);

            return page.Substring(position, nextPosition - position);
        }

        public void DownloadFile()
        {
            Logger.WriteLine("Start " + Article);

            int count = 0;
        retry:
            try
            {
                if (++count > MaxRetryCount)
                {
                    Logger.WriteLine("FileRetriever Download Failed - Retry Count Exceeded - " + Article);
                    throw new OperationCanceledException();
                }

                var request = (HttpWebRequest)WebRequest.Create(MediaUrl);
                request.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";

                var response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    Logger.WriteLine("FileRetriever Download Failed - Not Exist Media File - " + Article);
                    throw new FileNotFoundException();
                }
                else if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    Logger.WriteLine("FileRetriever Download Failed - Forbidden - " + Article);
                    throw new FileNotFoundException();
                }
                else if (response.StatusCode != HttpStatusCode.OK)
                    goto retry;

                var fileLength = response.ContentLength;

                using (var rs = response.GetResponseStream())
                {
                    using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                    {
                        byte[] data = new byte[4096];
                        long totalRead = 0;
                        while (totalRead < fileLength)
                        {
                            var read = rs.Read(data, 0, data.Length);
                            totalRead += read;
                            fs.Write(data, 0, read);
                        }
                    }
                }

                Logger.WriteLine("Finished " + Article);
            }
            catch (WebException ex)
            {
                switch (ex.Status)
                {
                    case WebExceptionStatus.Success:
                        break;
                    case WebExceptionStatus.Timeout:
                        goto retry;
                    default:
                        break;
                }
                Logger.WriteLine("Failed " + Article + " : " + ex.Message);
            }
            catch (IOException)
            {
                goto retry;
            }
        }
    }
}
