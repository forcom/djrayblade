﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace djrayblade
{
    public class ListRetriever
    {
        private const string ListUrl = @"http://blog.naver.com/PostTitleListAsync.nhn?blogId=djrayblade&viewdate=&categoryNo=0&parentCategoryNo=&countPerPage=30&currentPage=";

        public class JsonResponse
        {
            [JsonProperty("countPerPage")]
            public long CountPerPage { get; set; }

            [JsonProperty("totalCount")]
            public long TotalCount { get; set; }

            [JsonProperty("postList")]
            public List<Article> PostList { get; set; }
        }

        public static List<Article> GetArticleList()
        {
            var list = new List<Article>();

            var cli = new WebClient();

            Logger.WriteLine("Initializing article lists...");

            var info = JsonConvert.DeserializeObject<JsonResponse>(cli.DownloadString(ListUrl));

            Logger.WriteLine("Info: " + info.TotalCount + " articles, " + info.CountPerPage + " per page");

            for (var i = 0; i*info.CountPerPage < info.TotalCount; i++)
            {
                var curPage = JsonConvert.DeserializeObject<JsonResponse>(cli.DownloadString(ListUrl + (i + 1)));

                curPage.PostList = (from post in curPage.PostList
                    select new Article
                    {
                        Id = post.Id,
                        Title = HttpUtility.UrlDecode(post.Title)
                    }).ToList();

                list.AddRange(curPage.PostList);
            }

            list.Reverse();

            for (var i = 0; i < list.Count; i++)
            {
                Logger.WriteLine(i + " - " + list[i]);
            }

            Logger.WriteLine("Article list initialized.");

            return list;
        }
    }
}
