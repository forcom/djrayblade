﻿using Newtonsoft.Json;

namespace djrayblade
{
    public class Article
    {
        [JsonProperty("logNo")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        public override string ToString()
        {
            return "[" + Id + "] " + Title;
        }
    }
}
