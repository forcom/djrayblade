﻿using System;
using System.IO;
using System.Text;

namespace djrayblade
{
    public class Logger
    {
        private static StreamWriter writer;

        public static void Initialize()
        {
            writer = new StreamWriter("djrayblade.txt", true, Encoding.UTF8);
            writer.AutoFlush = true;

            WriteLine("Logger Initialized.");
        }

        public static void WriteLine(dynamic obj)
        {
            lock (writer)
            {
                writer.Write("[" + DateTime.Now.ToString() + "] ");
                writer.WriteLine(obj);
            }
        }
    }

}
