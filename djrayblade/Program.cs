﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace djrayblade
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.Initialize();

            Console.WriteLine("djrayblade downloader");
            Console.WriteLine("Initializing article lists...");
            List<Article> articleList = ListRetriever.GetArticleList();

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Total counts: " + articleList.Count);
                Console.WriteLine();
                Console.Write("From: ");
                // ReSharper disable once AssignNullToNotNullAttribute
                var from = int.Parse(Console.ReadLine());
                Console.Write("To: ");
                var line = Console.ReadLine();
                // ReSharper disable once AssignNullToNotNullAttribute
                var to = (line ?? string.Empty) == string.Empty ? articleList.Count : int.Parse(line);

                Logger.WriteLine("From " + from + " To " + to);

                DownloadFiles(articleList, from, to);

                Console.WriteLine("Finished Download.");
            }
        }

        private static void DownloadFiles(IEnumerable<Article> articleList, int from, int to)
        {
            int maxInstanceCount = int.Parse(ConfigurationManager.AppSettings.Get("MaxInstanceCount") ?? "10");

            long instanceCount = 0;
            Parallel.ForEach(articleList.Skip(from).Take(to - from + 1), article =>
            {
            retry:
                if (Interlocked.Read(ref instanceCount) <= maxInstanceCount)
                {
                    Interlocked.Increment(ref instanceCount);
                    try
                    {
                        var instance = new FileRetriever(article);
                        Console.WriteLine("    " + instance.Article);
                        instance.DownloadFile();
                    }
                    catch (FileNotFoundException)
                    {
                        Console.WriteLine("*** " + article);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("!!! " + article);
                    }
                    finally
                    {
                        Interlocked.Decrement(ref instanceCount);
                    }
                }
                else
                {
                    Thread.Sleep(2000);
                    goto retry;
                }
            });
        }
    }
}
